(function(){
   'use strict';

   angular
      .module('app')
      .controller('DemographicsController', DemographicsController)
      .directive('excelExport',
          function ($rootScope) {
             return {
               restrict: 'A',
               scope: {
                  fileName: "@",
                  data: "&exportData"
               },
               replace: true,
               template: '<button class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10" ng-click="download()"><i class="fa fa-cloud-download"></i> Export</button>',
               link: function (scope, element) {
                   scope.download = function() {

                     $rootScope.ExportCustomer();

                      
                   };
                }
             };
          });;

   DemographicsController.$inject = ['PreloaderService', 'QueryService', '$http', 'ChartService', 'TableService', 'ToastService', '$mdDialog', '$scope', 'ExportToastService', '$rootScope' ];
   function DemographicsController(PreloaderService, QueryService, $http, ChartService, TableService, ToastService, $mdDialog, $scope, ExportToastService, $rootScope) {
      var vm = this; 
        
      // Variables
      // $rootScope.exportCustomerData = [];

       vm.daterange = { 
         summary: { start: '', end: ''}
      };
 
      vm.email = "";  
      vm.gender = { title: "Gender", value: ""};
      vm.bday = { title: "Birth Month", value: ""};
      vm.agerange = { title: "Age Range", start: "", end: ""};
      vm.totalusers = 0;
      vm.gendercount = { male: 0, female: 0};

      vm.totalrecord = 0;
      vm.doneloading_transactions = true; // preloader
      vm.exportingprogress = false; 
      vm.customers = [];
    
      // Access Functions
      vm.SelectGender = SelectGender; 
      vm.SelectBirthday = SelectBirthday; 
      vm.SelectAgeRange = SelectAgeRange; 
      vm.GetCustomerDetails = GetCustomerDetails;  

      $('.date-picker').datepicker({ 
         orientation: "left",
         autoclose: true,
         clearBtn: true, 
         todayHighlight: true 
      }); 

      // Initialization
      Initialize();


      // Public functions
      function Initialize(){ 
         PreloaderService.Display();
         GetRegistration();
         GetAge();
         GetGender(); 
         PreloaderService.Hide(); 
         ShowDefaultData();
        
      }

      function GetRegistration(){
         QueryService.GetPlatformRegistration()
          .then( function(result){
            // console.log('result', result);
              if (result.response == "Success") { 
                  vm.totalusers = result.total_users;
              }
          });
      }

      function GetAge(){
         QueryService.GetAge()
            .then( function(result){   
               if (result.response == "Success") {
                // console.info(result.data);
                  ChartService.CreateBarChart_Single("ageChart", result.data, 
                      "label", "Age", "count", "Age Count: <br><span style='font-size:14px'><b>[[value]]</b></span>", 2, 2, false); 
               }
            });   
      }

      function GetGender(){ 
         QueryService.GetGender()
             .then( function(result){
               // console.log('result', result);
                 if (result.response == "Success") { 
                     angular.forEach(result.data, function(val){
                         if (val.gender == "male") { vm.gendercount.male = val.count; }
                         if (val.gender == "female") { vm.gendercount.female = val.count; } 
                     });   
                 }
             });
      }

      function ShowDefaultData(){
       QueryService.GetDemographics()
          .then( function(result){ 
               // console.info('demo', result.data.users);
               if (result.response == "Success") {  
                  vm.customers = result.data.users;
                   // ExportData();
                  vm.tableParams = TableService.Create(result.data.users, vm.tableParams); 
                  vm.totalrecord = result.data.users.length;
               }
               else if (result.response == "Empty"){ 
                  ToastService.Show('No Data Found', 'Oops! It seems that there\'s no existing record at the moment');
               }
               else{
                  ToastService.Show('Something went wrong', result); 
               } 
               vm.doneloading_transactions = true;
               
          });  
      }

      function GetCustomerDetails(){     

         if (vm.daterange.summary.start =="" && vm.daterange.summary.end =="" ) {  
            ToastService.Show('No Filter Found', 'Oops! You seem to forget selecting a filter. Kindly select at least 1 filter.'); 
            return;
         }
         
         vm.doneloading_transactions = false;
         vm.tableParams = TableService.Empty(vm.tableParams);
         QueryService.SearchDemographics(vm.daterange.summary.start, vm.daterange.summary.end)
             .then( function(result){ 
                  console.info('demo', result.data.users.length);
                  if (result.response == "Success") {  
                     vm.customers = result.data.users;
                      // ExportData();
                     vm.tableParams = TableService.Create(result.data.users, vm.tableParams); 
                     vm.totalrecord = result.data.users.length;
                  }
                  else if (result.response == "Empty"){ 
                     ToastService.Show('No Data Found', 'Oops! It seems that there\'s no existing record at the moment');
                  }
                  else{
                     ToastService.Show('Something went wrong', result); 
                  } 
                  vm.doneloading_transactions = true;
                  
             });  
      } 

      function SelectGender(_gender){
         if (_gender == "Male") { vm.gender = { title: "Male", value: "male"} }
         else if (_gender == "Female") { vm.gender = { title: "Female", value: "female"} }
         else { vm.gender = { title: "Gender", value: ""} } 
      }

      function SelectBirthday(_bday){
         switch(_bday){
             case 1: vm.bday = { title: "January", value: 1}; break;
             case 2: vm.bday = { title: "February", value: 2}; break;
             case 3: vm.bday = { title: "March", value: 3}; break;
             case 4: vm.bday = { title: "April", value: 4}; break;
             case 5: vm.bday = { title: "May", value: 5}; break;
             case 6: vm.bday = { title: "June", value: 6}; break;
             case 7: vm.bday = { title: "July", value: 7}; break;
             case 8: vm.bday = { title: "August", value: 8}; break;
             case 9: vm.bday = { title: "September", value: 9}; break;
             case 10: vm.bday = { title: "October", value: 10}; break;
             case 11: vm.bday = { title: "November", value: 11}; break;
             case 12: vm.bday = { title: "December", value: 12}; break;
             default: vm.bday = { title: "Birth Month", value: ""}; break;
         }
      }

      function SelectAgeRange(_agerange){
         switch(_agerange){
             case 1: vm.agerange = { title: "18 to 25", start: 18, end: 25}; break;
             case 2: vm.agerange = { title: "26 to 35", start: 26, end: 35}; break;
             case 3: vm.agerange = { title: "36 to 45", start: 36, end: 45}; break;
             case 4: vm.agerange = { title: "46 to 59", start: 46, end: 59}; break; 
             default: vm.agerange = { title: "Age Range", start: "", end: ""}; break;
         }
      }

      $rootScope.ExportCustomer = function(){

         // if (vm.daterange.summary.start == "" && vm.daterange.summary.end == "") {  
         //    QueryService.GetDemographics().then( function(result){ 
         //          if (result.data.users != "") {  
         //             vm.customers = result.data.users;
         //          }
         //          else if (result.data.users == ""){ 
         //             ToastService.Show('No Data Found', 'Oops! It seems that there\'s no existing record at the moment');
         //          }
         //          ExportCustomerData();
         //     }); 
         // }
         if (vm.daterange.summary.start =="" && vm.daterange.summary.end =="" ) {  
            ToastService.Show('No Filter Found', 'Oops! You seem to forget selecting a filter. Kindly select at least 1 filter.'); 
            return;
         }
         //else{
            QueryService.SearchDemographics(vm.daterange.summary.start, vm.daterange.summary.end).then( function(result){           
                  if (result.data.users.length != 0) {  
                     vm.customers = result.data.users;
                  }
                  else{ 
                     vm.customers = [];
                  }
                ExportCustomerData();   
             }); 
             
         }
           
      //}

      function ExportCustomerData(){
         var today = new Date();
         var dd = today.getDate();
         var mm = today.getMonth()+1; 
         var yy = today.getFullYear();
         var date = dd+ '-' + mm+ '-' + yy;
         var exportCustomerData = [];
         var fileName = date+"_demographics";

         exportCustomerData.push(["Email", "Name", "Birthday", "Gender", "Mobile", "Country", "Address", "Age", "Registered"]);
         angular.forEach(vm.customers, function(value, key) {
            exportCustomerData.push([value.email, value.name, value.birthdate, value.gender, value.mobile_number, value.country, value.address, value.age, value.created_at]);
         });

         function datenum(v, date1904) {
            if(date1904) v+=1462;
            var epoch = Date.parse(v);
            return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
         };

         function getSheet(data, opts) {
            var ws = {};
            var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
            for(var R = 0; R != data.length; ++R) {
               for(var C = 0; C != data[R].length; ++C) {
                  if(range.s.r > R) range.s.r = R;
                  if(range.s.c > C) range.s.c = C;
                  if(range.e.r < R) range.e.r = R;
                  if(range.e.c < C) range.e.c = C;
                  
                  var cell = {v: data[R][C] };
                  if(cell.v == null) continue;
                  var cell_ref = XLSX.utils.encode_cell({c:C,r:R});

                  if(typeof cell.v === 'number') cell.t = 'n';
                  else if(typeof cell.v === 'boolean') cell.t = 'b';
                  else if(cell.v instanceof Date) {
                  cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                  cell.v = datenum(cell.v);
                  }
                  else cell.t = 's';

                  ws[cell_ref] = cell;
               }
            }
               if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
               return ws;
            };

            function Workbook() {
               if(!(this instanceof Workbook)) return new Workbook();
               this.SheetNames = [];
               this.Sheets = {};
            }

            var wb = new Workbook(), ws = getSheet(exportCustomerData);
            /* add worksheet to workbook */
            wb.SheetNames.push(fileName);
            wb.Sheets[fileName] = ws;
            var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});

            function s2ab(s) {
               var buf = new ArrayBuffer(s.length);
               var view = new Uint8Array(buf);
               for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
               return buf;
            }

            saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), fileName+'.xlsx');
      }
      


   
   Metronic.init(); // init metronic core components
   Layout.init(); // init current layout   
   } 


})();

