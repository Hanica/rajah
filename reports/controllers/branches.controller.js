(function(){
   'use strict';

   angular
      .module('app')
      .controller('BranchesController', BranchesController)
      .directive('exportTransaction',
          function ($rootScope) {
             return {
               restrict: 'A',
               scope: {
                  fileName: "@",
                  data: "&exportTransaction"
               },
               replace: true,
               template: '<button class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10" ng-click="download()"><i class="fa fa-cloud-download"></i> Export</button>',
               link: function (scope, element) {
                  scope.download = function() {
                     $rootScope.ExportTransaction();
                  };
               }
             };
          })
         .directive('exportQuartervisit',
          function ($rootScope) {
             return {
               restrict: 'A',
               scope: {
                  fileName: "@",
                  data: "&exportQuartervisit"
               },
               replace: true,
               template: '<button class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10" ng-click="download()"><i class="fa fa-cloud-download"></i> Export</button>',
               link: function (scope, element) {
                  scope.download = function() {
                     $rootScope.ExportQuarterData('visits');
                  };
               }
             };
          })
         .directive('exportQuartertransaction',
          function ($rootScope) {
             return {
               restrict: 'A',
               scope: {
                  fileName: "@",
                  data: "&exportQuartertransaction"
               },
               replace: true,
               template: '<button class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10" ng-click="download()"><i class="fa fa-cloud-download"></i> Export</button>',
               link: function (scope, element) {
                  scope.download = function() {
                     $rootScope.ExportQuarterData('transactions');
                  };
               }
             };
          });

   BranchesController.$inject = ['$rootScope', 'PreloaderService', 'QueryService', 'ChartService', 'TableService', 'ToastService', '$mdDialog', '$scope', 'ExportToastService' ];
   function BranchesController($rootScope, PreloaderService, QueryService, ChartService, TableService, ToastService, $mdDialog, $scope, ExportToastService) {
      var vm = this; 
        
      // Variables 
      vm.daterange = { 
         summary: { start: '', end: ''}
      };
      vm.totalrecord = { summary: 0 };
      vm.preloader = {
         summary: true 
      };
      vm.exportingprogress = false; // export flag 
      
      // Access Functions 
      vm.GetBranchSummary = GetBranchSummary;
      vm.SearchBranchSummary = SearchBranchSummary;

      $('.date-picker').datepicker({ 
         orientation: "left",
         autoclose: true,
         clearBtn: true, 
         todayHighlight: true 
      }); 
      
      // Initialization
      Initialize();


      // Public functions
      function Initialize(){   
         PreloaderService.Display();
         PreloaderService.Hide(); 
         GetBranchQuarterlySales();
         GetBranchQuarterlyVisits();
         GetBranchSummary();
      }

      function GetBranchQuarterlySales(){
         QueryService.GetBranchQuarterlySales ()
            .then( function(result){
              // console.log('branch', result);
                  if (result.response == "Success") {  
                     vm.quarterlysales = result.data;
                  }
            });
      }

      function GetBranchQuarterlyVisits(){
         QueryService.GetBranchQuarterlyVisits ()
            .then( function(result){
               // console.info(result);
                  if (result.response == "Success") {  
                     vm.quarterlyvisit = result.data;
                  }
            });
      }

      function GetBranchSummary(){
         vm.tableParams = TableService.Empty(vm.tableParams);
         QueryService.GetBranchSummary()
            .then( function(result){ 
               if (result.data.suppliers != '') {  
                  vm.tableParams = TableService.Create(result.data.suppliers, vm.tableParams); 
                  vm.totalrecord.summary = result.data.suppliers.length;  
               }
               else if (result.data.suppliers == ''){ 
                  vm.totalrecord.summary = result.data.suppliers.length;
                  ToastService.Show('No Data Found', 'Oops! It seems that there\'s no existing record at the moment');
                  return;
               }
               else{
                  ToastService.Show('Something went wrong', result); 
                  return;
               } 
            }); 
      }

      function SearchBranchSummary(){
         // console.log('ypppp');
         if (vm.daterange.summary.start == "" && vm.daterange.summary.end == "") {  
            ToastService.Show('No Date Range Specified', 'Oops! Don\'t forget to specify a date'); 
            return;
         }

         vm.preloader.summary = false;
         vm.tableParams = TableService.Empty(vm.tableParams);
         QueryService.SearchBranchSummary(vm.daterange.summary.start, vm.daterange.summary.end)
            .then( function(result){ 
               console.log(result);
               if (result.data.suppliers != "") {  
                  vm.tableParams = TableService.Create(result.data.suppliers, vm.tableParams); 
                  vm.totalrecord.summary = result.data.suppliers.length;  
               }
               else if (result.data.suppliers == ""){ 
                  vm.totalrecord.summary = result.data.suppliers.length;  
                  ToastService.Show('No Data Found', 'Oops! It seems that there\'s no existing record at the moment');
               }
               else{
                  ToastService.Show('Something went wrong', result); 
               } 
               vm.preloader.summary = true;
            }); 
      }

      $rootScope.ExportTransaction = function(){

         // if (vm.daterange.summary.start == "" && vm.daterange.summary.end == "") {  
         //    QueryService.GetBranchSummary().then( function(result){ 
         //          if (result.data.suppliers != "") {  
         //             vm.transactions = result.data.suppliers;
         //          }
         //          else if (result.data.suppliers == ""){ 
         //             ToastService.Show('No Data Found', 'Oops! It seems that there\'s no existing record at the moment');
         //          }
         //          ExportDataTransactions();
         //     }); 
         // }
         if (vm.daterange.summary.start == "" && vm.daterange.summary.end == "") {  
            ToastService.Show('No Date Range Specified', 'Oops! Don\'t forget to specify a date'); 
            return;
         }
         // else{
            QueryService.SearchBranchSummary(vm.daterange.summary.start, vm.daterange.summary.end).then( function(result){           
                  if (result.data.suppliers.length != 0) {  
                     vm.transactions = result.data.suppliers;
                  }
                  else{ 
                     vm.transactions = [];
                  }
                ExportDataTransactions();   
             }); 
             // 
         //}
      }

      function ExportDataTransactions(){

         var today = new Date();
         var dd = today.getDate();
         var mm = today.getMonth()+1;
         var yy = today.getFullYear();
         var date = dd+ '-' + mm+ '-' + yy;
         var exportTransactionData = [];
         var fileName = date+"_transactionSummary";

         exportTransactionData.push(["Supplier", "Total Amount", "Customer Count", "Total Transaction"]);
         angular.forEach(vm.transactions, function(value, key) {
            exportTransactionData.push([value.supplier, value.totalAmount, value.totalMember, value.totalTransaction]);
         });

          function datenum(v, date1904) {
            if(date1904) v+=1462;
            var epoch = Date.parse(v);
            return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
          };
                         
          function getSheet(data, opts) {
             var ws = {};
             var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
             for(var R = 0; R != data.length; ++R) {
                for(var C = 0; C != data[R].length; ++C) {
                   if(range.s.r > R) range.s.r = R;
                   if(range.s.c > C) range.s.c = C;
                   if(range.e.r < R) range.e.r = R;
                   if(range.e.c < C) range.e.c = C;
                   var cell = {v: data[R][C] };
                   if(cell.v == null) continue;
                   var cell_ref = XLSX.utils.encode_cell({c:C,r:R});
                   
                   if(typeof cell.v === 'number') cell.t = 'n';
                   else if(typeof cell.v === 'boolean') cell.t = 'b';
                   else if(cell.v instanceof Date) {
                      cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                      cell.v = datenum(cell.v);
                   }
                   else cell.t = 's';
                   
                   ws[cell_ref] = cell;
                }
             }
             if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
             return ws;
          };
                         
          function Workbook() {
             if(!(this instanceof Workbook)) return new Workbook();
             this.SheetNames = [];
             this.Sheets = {};
          }
           
          var wb = new Workbook(), ws = getSheet(exportTransactionData);
          /* add worksheet to workbook */
          wb.SheetNames.push(fileName);
          wb.Sheets[fileName] = ws;
          var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});

          function s2ab(s) {
             var buf = new ArrayBuffer(s.length);
             var view = new Uint8Array(buf);
             for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
             return buf;
         }
          
         saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), fileName+'.xlsx');
      }


      $rootScope.ExportQuarterData = function(quarterData){

         var today = new Date();
         var dd = today.getDate();
         var mm = today.getMonth()+1;
         var yy = today.getFullYear();
         var date = dd+ '-' + mm+ '-' + yy;
         var exportQuarterData = [];
         var fileName = '';

         if(quarterData == 'visits'){
            fileName = date+"quarterlyVisit";
            exportQuarterData.push(["Supplier", "Sales"]);
            // console.log('vm.quarterlysales' , vm.quarterlysales);
            angular.forEach(vm.quarterlysales, function(value, key) {
               exportQuarterData.push([value.supplier, value.sales]);
            });
         }else if(quarterData == 'transactions'){
            fileName = date+"quarterlyTransaction";
            exportQuarterData.push(["Supplier", "Transactions"]);
            angular.forEach(vm.quarterlyvisit, function(value, key) {
               exportQuarterData.push([value.supplier, value.transactions]);
            });
         }
         

          function datenum(v, date1904) {
            if(date1904) v+=1462;
            var epoch = Date.parse(v);
            return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
          };
                         
          function getSheet(data, opts) {
             var ws = {};
             var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
             for(var R = 0; R != data.length; ++R) {
                for(var C = 0; C != data[R].length; ++C) {
                   if(range.s.r > R) range.s.r = R;
                   if(range.s.c > C) range.s.c = C;
                   if(range.e.r < R) range.e.r = R;
                   if(range.e.c < C) range.e.c = C;
                   var cell = {v: data[R][C] };
                   if(cell.v == null) continue;
                   var cell_ref = XLSX.utils.encode_cell({c:C,r:R});
                   
                   if(typeof cell.v === 'number') cell.t = 'n';
                   else if(typeof cell.v === 'boolean') cell.t = 'b';
                   else if(cell.v instanceof Date) {
                      cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                      cell.v = datenum(cell.v);
                   }
                   else cell.t = 's';
                   
                   ws[cell_ref] = cell;
                }
             }
             if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
             return ws;
          };
                         
          function Workbook() {
             if(!(this instanceof Workbook)) return new Workbook();
             this.SheetNames = [];
             this.Sheets = {};
          }
           
          var wb = new Workbook(), ws = getSheet(exportQuarterData);
          /* add worksheet to workbook */
          wb.SheetNames.push(fileName);
          wb.Sheets[fileName] = ws;
          var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});

          function s2ab(s) {
             var buf = new ArrayBuffer(s.length);
             var view = new Uint8Array(buf);
             for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
             return buf;
         }
          
         saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), fileName+'.xlsx');
      }

        
      Metronic.init(); // init metronic core components
      Layout.init(); // init current layout   
   } 


})();

