(function(){
   'use strict';

   angular
      .module('app')
      .controller('RedemptionController', RedemptionController)
      .directive('exportRedemption',
          function ($rootScope) {
             return {
               restrict: 'A',
               scope: {
                  fileName: "@",
                  data: "&exportSupplierRData"
               },
               replace: true,
               template: '<button class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10" ng-click="download()"><i class="fa fa-cloud-download"></i> Export</button>',
               link: function (scope, element) {
                  scope.download = function() {
                     $rootScope.ExportSupplierRedemption();
                  };
               }
             };
          })
      .directive('exportCustomer',
          function ($rootScope) {
             return {
               restrict: 'A',
               scope: {
                  fileName: "@",
                  data: "&exportCustomerRData"
               },
               replace: true,
               template: '<button class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10" ng-click="download()"><i class="fa fa-cloud-download"></i> Export</button>',
               link: function (scope, element) {
                  scope.download = function() {
                     $rootScope.ExportCustomerRedemption();
                  };
                }
             };
          });;

   RedemptionController.$inject = ['$rootScope', 'PreloaderService', 'QueryService', 'ChartService', 'TableService', 'ToastService', '$mdDialog', '$scope', 'ExportToastService' ];
   function RedemptionController($rootScope, PreloaderService, QueryService, ChartService, TableService, ToastService, $mdDialog, $scope, ExportToastService) {
      var vm = this; 
        
      // Variables 
      vm.daterange = { 
         branch: { start: '', end: ''},
         customer: { start: '', end: ''}
      };
      vm.totalrecord = { branch: 0, customer: 0 };
      vm.preloader = {
         branch: true,
         customer: true
      };
      vm.exportingprogress = false; // export flag 
      // vm.supplierRedemption = [];
      $rootScope.exportSupplierRData = [];
      $rootScope.exportCustomerRData = [];
      
      // Access Functions 
      vm.ShowSupplierRedemption = ShowSupplierRedemption;
      vm.ShowCustomerRedemption = ShowCustomerRedemption;
      
      $('.date-picker').datepicker({ 
         orientation: "left",
         autoclose: true,
         clearBtn: true, 
         todayHighlight: true 
      });  
      
      // Initialization
      Initialize();


      // Public functions
      function Initialize(){   
         PreloaderService.Display();
         PreloaderService.Hide(); 
         GetSupplierRedemption();
         GetCustomerRedemption();
      }

      /************ SUPPLIER FUNCTIONS *******************/
      
      function GetSupplierRedemption(){
          QueryService.GetSupplierRedemption()
            .then( function(result){  
               // console.log('result' , result);
               if (result.response == "Success") {  
                  // vm.supplierRedemption = result.data;
                  vm.tableParams_branch = TableService.Create(result.data, vm.tableParams_branch); 
                  vm.totalrecord.branch = result.data.length;
               }
               else if (result.response == "Empty"){ 
                  vm.totalrecord.branch = result.data.length;
                  ToastService.Show('No Data Found', 'Oops! It seems there\' no records at the moment');
                  return;
               }
                  
            });  
      }

      function ShowSupplierRedemption(){
         if (vm.daterange.branch.start == "" && vm.daterange.branch.end == "") {  
            ToastService.Show('No Date Range Specified', 'Oops! Don\'t forget to specify a date'); 
            return;
         }
         
         vm.preloader.branch = false;
         vm.tableParams_branch = TableService.Empty(vm.tableParams_branch);
         QueryService.ShowSupplierRedemption(vm.daterange.branch.start, vm.daterange.branch.end)
            .then( function(result){  
               console.info(result);
               if (result.response == "Success") {  
                  if (result.data != "") {  
                     vm.supplierRedemption = result.data;
                     vm.tableParams_branch = TableService.Create(result.data, vm.tableParams_branch); 
                     vm.totalrecord.branch = result.data.length;
                  }else{
                     vm.supplierRedemption = [];
                     vm.totalrecord.branch = result.data.length;
                     // ToastService.Show('No Data Found', 'Oops! It seems there\' no records at the moment');
                  }
               }
               else if (result.response == "Empty"){ 
                  ToastService.Show('No Data Found', 'Oops! It seems there\' no records at the moment');
               }
               vm.preloader.branch = true;
                  
            });  
      }

      $rootScope.ExportSupplierRedemption = function(){
         // console.log('exporting....');
         $rootScope.exportSupplierRData = [];
         // vm.supplierRedemption = [];
         // if (vm.daterange.branch.start == "" && vm.daterange.branch.end == "") {  
         //    QueryService.GetSupplierRedemption().then( function(result){ 
         //          if (result.response == "Success") {  
         //             vm.supplierRedemption = result.data;
         //             // console.info('supply: ' , vm.supplierRedemption);
         //          }
         //          else if (result.response == "Empty"){ 
         //             ToastService.Show('No Data Found', 'Oops! It seems that there\'s no existing record at the moment');
         //          }
         //          else{
         //             ToastService.Show('Something went wrong', result); 
         //          } 
         //          ExportDataSupplier();
         //     }); 
         // }else{
            if (vm.daterange.branch.start == "" && vm.daterange.branch.end == "") {  
               ToastService.Show('No Date Range Specified', 'Oops! Don\'t forget to specify a date'); 
               return;
            }
            // ShowSupplierRedemption();
            QueryService.ShowSupplierRedemption(vm.daterange.branch.start, vm.daterange.branch.end).then( function(result){           
                  if (result.response == "Success") {  
                     // console.log('length: ' , result.data);
                     if(result.data.length == 0){
                        vm.supplierRedemption = [];
                        // console.info('supply2: ' , vm.supplierRedemption);
                     }else{
                        vm.supplierRedemption = result.data;
                        // console.info('supply3: ' , vm.supplierRedemption);
                     }
                  }
                  else if (result.response == "Empty"){ 
                     ToastService.Show('No Data Found', 'Oops! It seems that there\'s no existing record at the moment');
                  }
                  else{
                     ToastService.Show('Something went wrong', result); 
                  } 
                ExportDataSupplier();   
             }); 
             
        // }
           
         // var today = new Date();
         // var dd = today.getDate();
         // var mm = today.getMonth()+1;
         // var yy = today.getFullYear();
         // var date = dd+ '-' + mm+ '-' + yy;

         // $rootScope.fileName = date+"_supplierRedemption";
         // $rootScope.exportSupplierRData.push(["Item", "Physical Card", "Virtual Card", "Total Card Redeemed", "Card Revenue"]);
         // angular.forEach(vm.supplierRedemption, function(value, key) {
         //    $rootScope.exportSupplierRData.push([value.supplier, value.physicalCard, value.virtualCard, value.totalCard, value.cardRevenue]);
         // });
      }

      function ExportDataSupplier(){

         var today = new Date();
         var dd = today.getDate();
         var mm = today.getMonth()+1;
         var yy = today.getFullYear();
         var date = dd+ '-' + mm+ '-' + yy;

         $rootScope.fileName = date+"_supplierRedemption";
         $rootScope.exportSupplierRData.push(["Item", "Physical Card", "Virtual Card", "Total Card Redeemed", "Card Revenue"]);
         angular.forEach(vm.supplierRedemption, function(value, key) {
            $rootScope.exportSupplierRData.push([value.supplier, value.physicalCard, value.virtualCard, value.totalCard, value.cardRevenue]);
         });

          function datenum(v, date1904) {
            if(date1904) v+=1462;
            var epoch = Date.parse(v);
            return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
          };
                         
          function getSheet(data, opts) {
             var ws = {};
             var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
             for(var R = 0; R != data.length; ++R) {
                for(var C = 0; C != data[R].length; ++C) {
                   if(range.s.r > R) range.s.r = R;
                   if(range.s.c > C) range.s.c = C;
                   if(range.e.r < R) range.e.r = R;
                   if(range.e.c < C) range.e.c = C;
                   var cell = {v: data[R][C] };
                   if(cell.v == null) continue;
                   var cell_ref = XLSX.utils.encode_cell({c:C,r:R});
                   
                   if(typeof cell.v === 'number') cell.t = 'n';
                   else if(typeof cell.v === 'boolean') cell.t = 'b';
                   else if(cell.v instanceof Date) {
                      cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                      cell.v = datenum(cell.v);
                   }
                   else cell.t = 's';
                   
                   ws[cell_ref] = cell;
                }
             }
             if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
             return ws;
          };
                         
          function Workbook() {
             if(!(this instanceof Workbook)) return new Workbook();
             this.SheetNames = [];
             this.Sheets = {};
          }
           
          var wb = new Workbook(), ws = getSheet($rootScope.exportSupplierRData);
          /* add worksheet to workbook */
          wb.SheetNames.push($rootScope.fileName);
          wb.Sheets[$rootScope.fileName] = ws;
          var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});

          function s2ab(s) {
             var buf = new ArrayBuffer(s.length);
             var view = new Uint8Array(buf);
             for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
             return buf;
         }
          
         saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), $rootScope.fileName+'.xlsx');
      }

      /************ CUSTOMER FUNCTIONS *******************/

      function GetCustomerRedemption(){
         QueryService.GetCustomerRedemption()
            .then( function(result){  
               if (result.response == "Success") {  
                  // console.log('recipient', result);
                  // vm.customerRedemption = result.data;
                  vm.tableParams_customer = TableService.Create(result.data, vm.tableParams_customer); 
                  vm.totalrecord.customer = result.data.length;
               }
               else if (result.response == "Empty"){ 
                  ToastService.Show('No Data Found', 'Oops! It seems there\' no records at the moment');
               }
               vm.preloader.customer = true;
                  
            });  
      }

      function ShowCustomerRedemption(){
         if (vm.daterange.customer.start == "" && vm.daterange.customer.end == "") {  
            ToastService.Show('No Date Range Specified', 'Oops! Don\'t forget to specify a date'); 
            return;
         }
         
         vm.preloader.customer = false;
         vm.tableParams_customer = TableService.Empty(vm.tableParams_customer);
         QueryService.ShowCustomerRedemption(vm.daterange.customer.start, vm.daterange.customer.end)
            .then( function(result){  
               console.info(result);
               if (result.response == "Success") { 
                  if (result.data != ""){
                     // vm.customerRedemption = result.data; 
                     vm.tableParams_customer = TableService.Create(result.data, vm.tableParams_customer); 
                     vm.totalrecord.customer = result.data.length;
                  }else{
                     vm.totalrecord.customer = result.data.length;
                     ToastService.Show('No Data Found', 'Oops! It seems there\' no records at the moment');
                  }
               }
               else { 
                  ToastService.Show('No Data Found', 'Oops! It seems there\' no records at the moment');
               }
               vm.preloader.customer = true;
                  
            });  
      }

      $rootScope.ExportCustomerRedemption = function(){
         $rootScope.exportCustomerRData = [];

         // if (vm.daterange.customer.start == "" && vm.daterange.customer.end == "") {  
         //    QueryService.GetCustomerRedemption()
         //     .then( function(result){ 
         //          if (result.response == "Success") {  
         //             vm.customerRedemption = result.data;
         //          }
         //          else if (result.response == "Empty"){ 
         //             ToastService.Show('No Data Found', 'Oops! It seems that there\'s no existing record at the moment');
         //          }
         //          else{
         //             ToastService.Show('Something went wrong', result); 
         //          } 
         //          ExportDataCustomer();
         //    });  
         // }else{
          if (vm.daterange.customer.start == "" && vm.daterange.customer.end == "") {  
            ToastService.Show('No Date Range Specified', 'Oops! Don\'t forget to specify a date'); 
            return;
         }
            QueryService.ShowCustomerRedemption(vm.daterange.customer.start, vm.daterange.customer.end)
             .then( function(result){ 
                  if (result.response == "Success") {  
                     if(result.data.length == 0){
                        vm.customerRedemption = [];
                     }else{
                        vm.customerRedemption = result.data;
                     }
                  }
                  else if (result.response == "Empty"){ 
                     ToastService.Show('No Data Found', 'Oops! It seems that there\'s no existing record at the moment');
                  }
                  else{
                     ToastService.Show('Something went wrong', result); 
                  } 
                  ExportDataCustomer();
            });  
        // }
         
      }

      function ExportDataCustomer (){
         var today = new Date();
         var dd = today.getDate();
         var mm = today.getMonth()+1;
         var yy = today.getFullYear();
         var date = dd+ '-' + mm+ '-' + yy;

         $rootScope.fileName = date+"_customerRedemption";
         $rootScope.exportCustomerRData.push(["Name of Purchaser", "Name of Recipient", "Product Purchaser", "Amount", "Gift Card Number", "Type of Card"]);
         angular.forEach(vm.customerRedemption, function(value, key) {
            $rootScope.exportCustomerRData.push([value.purchaser, value.recipient, value.productPurchased, value.amount, value.giftCard, value.cardType]);
         });

         function datenum(v, date1904) {
             if(date1904) v+=1462;
             var epoch = Date.parse(v);
             return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
          };
          
          function getSheet(data, opts) {
             var ws = {};
             var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
             for(var R = 0; R != data.length; ++R) {
                for(var C = 0; C != data[R].length; ++C) {
                   if(range.s.r > R) range.s.r = R;
                   if(range.s.c > C) range.s.c = C;
                   if(range.e.r < R) range.e.r = R;
                   if(range.e.c < C) range.e.c = C;
                   var cell = {v: data[R][C] };
                   if(cell.v == null) continue;
                   var cell_ref = XLSX.utils.encode_cell({c:C,r:R});
                   
                   if(typeof cell.v === 'number') cell.t = 'n';
                   else if(typeof cell.v === 'boolean') cell.t = 'b';
                   else if(cell.v instanceof Date) {
                      cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                      cell.v = datenum(cell.v);
                   }
                   else cell.t = 's';
                   
                   ws[cell_ref] = cell;
                }
             }
             if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
             return ws;
          };
          
          function Workbook() {
             if(!(this instanceof Workbook)) return new Workbook();
             this.SheetNames = [];
             this.Sheets = {};
          }
           
          var wb = new Workbook(), ws = getSheet($rootScope.exportCustomerRData);
          /* add worksheet to workbook */
          wb.SheetNames.push($rootScope.fileName);
          wb.Sheets[$rootScope.fileName] = ws;
          var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});

          function s2ab(s) {
             var buf = new ArrayBuffer(s.length);
             var view = new Uint8Array(buf);
             for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
             return buf;
         }
          
         saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), $rootScope.fileName+'.xlsx');
      }

      
      
        
      Metronic.init(); // init metronic core components
      Layout.init(); // init current layout   
   }


})();

