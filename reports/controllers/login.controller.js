﻿(function () {
    'use strict';

    angular
        .module('app') 
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'AuthenticationService', 'ToastService', '$rootScope'];
    function LoginController($location, AuthenticationService, ToastService, $rootScope) {
        var vm = this;

        vm.login = login;

        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials(); 
        })();

        function login() {
 
            // vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (response) { 
                vm.dataLoading = false;
                // console.log('response', response);
                if(response.success == true){
                    // console.log('access_token: ' +  response.user.access_types);
                    AuthenticationService.SetCredentials(vm.username, vm.password, response.user.access_token, response.user.access_types);
                    // $location.path('/');
                    // console.info(response.user);
                    var tabs = response.user.access_types;

                    switch(tabs[0]){
                        case 'demographics':
                            $location.path('/'); break;
                        case 'sales':
                            $location.path('/sales'); break;
                        case 'branches':
                            $location.path('/branches'); break;
                        case 'loyalty':
                            $location.path('/sales'); break;
                        case 'redemption':
                            $location.path('/redemption'); break;
                        default:
                            $location.path('/');
                    }
                } else {    
                    ToastService.Show('Error Login Credentials', ''); 
                    vm.dataLoading = false;
                }

            });
        };

        $(".login-bg").backstretch([
          "assets/img/bg.png?123"
          // "assets/img/login2.jpg?123", 
          // "assets/img/login3.jpg?123",
          // "assets/img/login4.jpg?123"  
        ], {
            fade: 750,
            duration: 4000
        });
    } 


})();
