(function(){
   'use strict';

   angular
      .module('app')
      .controller('SalesController', SalesController)
      .directive('exportSales',
          function ($rootScope) {
             return {
               restrict: 'A',
               scope: {
                  fileName: "@",
                  data: "&exportSalesSummary"
               },
               replace: true,
               template: '<button class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10" ng-click="download()"><i class="fa fa-cloud-download"></i> Export</button>',
               link: function (scope, element) {
                  scope.download = function() {
                     $rootScope.ExportSales();
                  };
               }
             };
          });

   SalesController.$inject = ['$rootScope', 'PreloaderService', 'QueryService', 'ChartService', 'TableService', 'ToastService', '$mdDialog', '$scope', 'ExportToastService' ];
   function SalesController($rootScope, PreloaderService, QueryService, ChartService, TableService, ToastService, $mdDialog, $scope, ExportToastService) {
      var vm = this; 
        
      // Variables 

      vm.daterange = { 
         summary: { start: '', end: ''}
      };
      vm.totalrecord = { summary: 0};
      vm.preloader = {
         summary: true
      }; 
      vm.exportingprogress = false; // export flag

      vm.email = "";  
      vm.cardno = "";  
      vm.fname = "";  
      vm.lname = "";  

      // Access Functions 
      vm.GetSalesSummary = GetSalesSummary;  
      vm.SearchSalesSummary = SearchSalesSummary;

      $('.date-picker').datepicker({ 
         orientation: "left",
         autoclose: true,
         clearBtn: true, 
         todayHighlight: true 
      });  
      
      // Initialization
      Initialize();


      // Public functions
      function Initialize(){   
         PreloaderService.Display();
         PreloaderService.Hide(); 
         GetSalesOverview(); 
         GetSalesSummary();
      } 
 
      function GetSalesOverview(){ 
         QueryService.GetSalesOverview()
             .then( function(result){
               // console.info(result.data);
                 if (result.response == "Success") { 
                     var config = [{
                            "id": "AmGraph-1", 
                            "title": "Total Sales",
                            "balloonText": "[[category]]<br><b><span style='font-size:14px;'>Sales:[[value]]</span></b>",
                            "bullet": "round",
                            "dashLength": 3,
                            "colorField":"color",
                            "valueField": "sales"
                        },{
                            "id": "AmGraph-2", 
                            "title": "Total Points",
                            "balloonText": "[[category]]<br><b><span style='font-size:14px;'>Points:[[value]]</span></b>",
                            "bullet": "round",
                            "dashLength": 3, 
                            "valueField": "points"
                        }];
                     ChartService.CreateLineChart_Multiple("saleschart", result.data, "month", config);
                 } 
             });
      }
   
      function GetSalesSummary(){  
         
         vm.tableParams = TableService.Empty(vm.tableParams);
         QueryService.GetSalesSummary()
             .then( function(result){ 
                  // console.log(result);
                  if (result.data.transactions != "") {  
                     vm.tableParams = TableService.Create(result.data.transactions, vm.tableParams); 
                     vm.totalrecord.summary = result.data.transactions.length;
                  }
                  else if (result.data.transactions == ""){ 
                     ToastService.Show('No Data Found', 'Oops! It seems there\' no records at the moment');
                  }
             });  
      }

      function SearchSalesSummary(){  
         if (vm.daterange.summary.start == "" && vm.daterange.summary.end == "" ) {  
            ToastService.Show('No Filter Found', 'Oops! You seem to forget selecting a filter. Kindly select at least 1 filter.'); 
            return;
         }
         
         vm.preloader.summary = false;
         vm.tableParams = TableService.Empty(vm.tableParams);
         QueryService.SearchSalesSummary(vm.daterange.summary.start, vm.daterange.summary.end)
             .then( function(result){ 
                  console.log(result);
                  if (result.data.transactions != "") {  
                     vm.tableParams = TableService.Create(result.data.transactions, vm.tableParams); 
                     vm.totalrecord.summary = result.data.transactions.length;
                  }
                  else if (result.data.transactions == ""){ 
                     vm.totalrecord.summary = result.data.transactions.length;
                     ToastService.Show('No Data Found', 'Oops! It seems there\' no records at the moment');
                  }
                  vm.preloader.summary = true;
                  
             });  
      }

       $rootScope.ExportSales = function(){
         $rootScope.exportSalesData = [];

         // if (vm.daterange.summary.start == "" && vm.daterange.summary.end == "") {  
         //    QueryService.GetSalesSummary().then( function(result){ 
         //          if (result.data.transactions != "") {  
         //             vm.sales = result.data.transactions;
         //          }
         //          else if (result.data.transactions == ""){ 
         //             ToastService.Show('No Data Found', 'Oops! It seems that there\'s no existing record at the moment');
         //          }
         //          ExportDataSales();
         //     }); 
         // }else{
            if (vm.daterange.summary.start == "" && vm.daterange.summary.end == "") {  
               ToastService.Show('No Date Range Specified', 'Oops! Don\'t forget to specify a date'); 
               return;
            }
            QueryService.SearchSalesSummary(vm.daterange.summary.start, vm.daterange.summary.end).then( function(result){           
                  if (result.data.transactions.length != 0) {  
                     vm.sales = result.data.transactions;
                  }
                  else{ 
                     vm.sales = [];
                  }
                  // else{
                  //    ToastService.Show('Something went wrong', result); 
                  // } 
                ExportDataSales();   
             }); 
             
        // }
           
      }

      function ExportDataSales(){

         var today = new Date();
         var dd = today.getDate();
         var mm = today.getMonth()+1;
         var yy = today.getFullYear();
         var date = dd+ '-' + mm+ '-' + yy;

         $rootScope.fileName = date+"_sales";
         $rootScope.exportSalesData.push(["Item", "Total Cards Bought", "Total Sales", "Total Redeemed Amount"]);
         angular.forEach(vm.sales, function(value, key) {
            $rootScope.exportSalesData.push([value.item, value.totalCards, value.totalSales, value.totalRedeemedAmount]);
         });

          function datenum(v, date1904) {
            if(date1904) v+=1462;
            var epoch = Date.parse(v);
            return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
          };
                         
          function getSheet(data, opts) {
             var ws = {};
             var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
             for(var R = 0; R != data.length; ++R) {
                for(var C = 0; C != data[R].length; ++C) {
                   if(range.s.r > R) range.s.r = R;
                   if(range.s.c > C) range.s.c = C;
                   if(range.e.r < R) range.e.r = R;
                   if(range.e.c < C) range.e.c = C;
                   var cell = {v: data[R][C] };
                   if(cell.v == null) continue;
                   var cell_ref = XLSX.utils.encode_cell({c:C,r:R});
                   
                   if(typeof cell.v === 'number') cell.t = 'n';
                   else if(typeof cell.v === 'boolean') cell.t = 'b';
                   else if(cell.v instanceof Date) {
                      cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                      cell.v = datenum(cell.v);
                   }
                   else cell.t = 's';
                   
                   ws[cell_ref] = cell;
                }
             }
             if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
             return ws;
          };
                         
          function Workbook() {
             if(!(this instanceof Workbook)) return new Workbook();
             this.SheetNames = [];
             this.Sheets = {};
          }
           
          var wb = new Workbook(), ws = getSheet($rootScope.exportSalesData);
          /* add worksheet to workbook */
          wb.SheetNames.push($rootScope.fileName);
          wb.Sheets[$rootScope.fileName] = ws;
          var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});

          function s2ab(s) {
             var buf = new ArrayBuffer(s.length);
             var view = new Uint8Array(buf);
             for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
             return buf;
         }
          
         saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), $rootScope.fileName+'.xlsx');
      }


        
      Metronic.init(); // init metronic core components
      Layout.init(); // init current layout   
   }  
 
 

})();

