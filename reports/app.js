﻿(function () {
    'use strict';

    angular
        .module('app', ['ngRoute', 'ngCookies', 'ngTable', 'ngAnimate', 'ngMaterial', 'ngMessages', 'material.svgAssetsCache', 'countTo']) 
        .config(config)
        .constant('DataLink', {
            "merchant_domain" : "http://124.106.33.31/project/rajah/"
            // "report_link" : "php/api.data.php?", 
            // "export_link" : "php/api.export.php?"
        }) 
        .filter('InitUpperCase', InitUpperCase)
        .run(run); 

    config.$inject = ['$routeProvider', '$locationProvider', '$mdThemingProvider', '$httpProvider'];
    function config($routeProvider, $locationProvider, $mdThemingProvider, $httpProvider) {
        $routeProvider
            .when('/', {
                cache: false,
                controller: 'DemographicsController',
                templateUrl: 'views/demographics.view.html',
                activetab: 'demographics',
                controllerAs: 'vm'
            })
            .when('/sales', { 
                cache: false,
                controller: 'SalesController',
                templateUrl: 'views/sales.view.html',
                activetab: 'sales',
                controllerAs: 'vm'
            })
            .when('/branches', { 
                cache: false,
                controller: 'BranchesController',
                templateUrl: 'views/branches.view.html',
                activetab: 'branches',
                controllerAs: 'vm'
            })
            .when('/redemption', { 
                cache: false,
                controller: 'RedemptionController',
                templateUrl: 'views/redemptions.view.html',
                activetab: 'redemption',
                controllerAs: 'vm'
            })
            .when('/login', {
                cache: false,
                controller: 'LoginController',
                templateUrl: 'views/login.view.html',
                activetab: 'login',
                controllerAs: 'vm'
            }) 

            .otherwise({ redirectTo: '/' });

            // $locationProvider.html5Mode(true);
        
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        var pomegranate = $mdThemingProvider.extendPalette('yellow', {
            '500': '#f5ca00'
        });
 
        // Register the new color palette map with the name <code>neonRed</code>
        $mdThemingProvider.definePalette('pomegranate', pomegranate);

        // Use that theme for the primary intentions
        $mdThemingProvider.theme('default')
            .primaryPalette('pomegranate');
    }

    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http', '$route'];
    function run($rootScope, $location, $cookieStore, $http, $route) { 
        
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) { 

            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
            var loggedIn = $rootScope.globals.currentUser;

            // console.info('restricted: ', restrictedPage);

            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
            else{
                var pages = $rootScope.globals.currentUser.access;
                // var currentLocation = $location.path();
                // var accessible = pages.indexOf(currentLocation);
                // console.info(pages);
                $rootScope.demo = pages.indexOf("demographics");
                $rootScope.sales = pages.indexOf("sales_report");
                $rootScope.branches = pages.indexOf("top_suppliers");
                $rootScope.redemption = pages.indexOf("redemption");

                // console.log("demographics: " + $rootScope.demo, "sales_report: " + $rootScope.sales, "top_suppliers: " + $rootScope.branches, "redemption: " + $rootScope.redemption);
               
            }


 
            if ($location.path() != '/login') {
                document.getElementById('header-container').className = 'page-header'; 
                document.getElementById('footer-container').className = 'page-footer'; 
            }
            else{
                document.getElementById('header-container').className = 'hide-element';
                document.getElementById('footer-container').className = 'hide-element';                 
            }
        });  

        $rootScope.$route = $route;   
    }  

    function InitUpperCase() {
        return function(input){ 
            
            if (!input) return;

            if(input.indexOf(' ') !== -1){
              var inputPieces,
                  i; 

              input = input.toLowerCase();
              inputPieces = input.split(' ');

              for(i = 0; i < inputPieces.length; i++){
                inputPieces[i] = capitalizeString(inputPieces[i]); 
              }

              return inputPieces.toString().replace(/,/g, ' ');
            }
            else {
              input = input.toLowerCase(); 
              return capitalizeString(input);
            }

            function capitalizeString(inputString){
              return inputString.substring(0,1).toUpperCase() + inputString.substring(1);
            }
        };
    }

})();
