(function () {
    'use strict';

    angular
        .module('app')


        // Angular File Upload module does not include this directive
        // Only for example


        /**
        * The ng-thumb directive
        * @author: nerv
        * @version: 0.1.2, 2014-01-09
        */
        .directive('errSrc', function() {
            return {
                link: function(scope, element, attrs) {
                    element.bind('error', function() {
                        if (attrs.src != attrs.errSrc) {
                            attrs.$set('src', attrs.errSrc);
                        }
                    });
                }
            }
        });

})();