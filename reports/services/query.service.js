(function(){
	'use strict';

	angular
		.module('app')
		.factory('QueryService', QueryService);

	QueryService.$inject = ['$http', '$rootScope', 'DataLink', 'ToastService'];
	function QueryService($http, $rootScope, DataLink, ToastService){
		var service = {};

		// Demographics
      service.GetAge = GetAge; //using
		service.GetPlatformRegistration = GetPlatformRegistration; //using
		service.GetGender = GetGender; //using
		service.GetDemographics = GetDemographics; //using
      service.SearchDemographics = SearchDemographics; //using

      // Sales
      service.GetSalesOverview = GetSalesOverview; //using
      service.GetSalesSummary = GetSalesSummary; //using
      service.SearchSalesSummary = SearchSalesSummary; //using

      // Branches
      service.GetBranchQuarterlySales = GetBranchQuarterlySales; //using
      service.GetBranchQuarterlyVisits = GetBranchQuarterlyVisits; //using
      service.GetBranchSummary = GetBranchSummary; //using
      service.SearchBranchSummary= SearchBranchSummary; //using

      // Redemption
      service.GetSupplierRedemption = GetSupplierRedemption; //using
      service.ShowSupplierRedemption = ShowSupplierRedemption; //using
      service.GetCustomerRedemption = GetCustomerRedemption; //using
      service.ShowCustomerRedemption = ShowCustomerRedemption; //using

      // Json
      service.GetTableRecords = GetTableRecords;
      service.GetRecord = GetRecord;


		return service;

      /*
			Public Functions
			----------------
			Demographics
      */

      function GetPlatformRegistration(){ //using
         return $http.get('http://52.77.228.111/api/v1/users/total_users', 
                { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant',  Accept : "application/json"} })
               .then(handleSuccess, function(error){ 
               ToastService.Show("An unexpected error occured", error); 
               });    
      }

      function GetAge(){ //using 
         return $http.get('http://52.77.228.111/api/v1/users/age_group', 
                { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant'} })
               .then(handleSuccess, function(error){ 
               ToastService.Show("An unexpected error occured", error); 
               });         
      }

      function GetGender(){ //using  
         return $http.get('http://52.77.228.111/api/v1/users/gender_group', 
                { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant'} })
               .then(handleSuccess, function(error){ 
               ToastService.Show("An unexpected error occured", error); 
               });    	
      }

		function GetDemographics(){   //using
         return $http.get('http://52.77.228.111/api/v1/users', 
             { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant',  Accept : "application/json"} })
            .then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
            });  
		} 

      function SearchDemographics(startDate, endDate){   //using
         return $http.get('http://52.77.228.111/api/v1/users/?search[startDate]='+startDate+'&search[endDate]='+endDate, 
             { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant',  Accept : "application/json"} })
            .then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
            });  
      } 

      /*
         Public Functions
         ----------------
         Loyalty Sales
      */
      function GetSalesOverview(){ //using
         return $http.get('http://52.77.228.111/api/v1/transactions/sales', 
                { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant',  Accept : "application/json"} })
               .then(handleSuccess, function(error){ 
               ToastService.Show("An unexpected error occured", error); 
               }); 
      }

      function GetSalesSummary(){ //using
         return $http.get('http://52.77.228.111/api/v1/transactions/', 
                { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant',  Accept : "application/json"} })
               .then(handleSuccess, function(error){ 
               ToastService.Show("An unexpected error occured", error); 
               });        
      }

      function SearchSalesSummary(startDate, endDate){ //using
         return $http.get('http://52.77.228.111/api/v1/transactions/?search[startDate]='+startDate+'&search[endDate]='+endDate, 
             { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant',  Accept : "application/json"} })
            .then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
            });          
      }

 

      /*
         Public Functions
         ----------------
         Branches
      */
      function GetBranchQuarterlySales(){ //using

          return $http.get('http://52.77.228.111/api/v1/suppliers/quarterly_sales', 
                { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant',  Accept : "application/json"} })
               .then(handleSuccess, function(error){ 
               ToastService.Show("An unexpected error occured", error); 
               }); 
      }

      function GetBranchQuarterlyVisits(){ //using
         
         return $http.get('http://52.77.228.111/api/v1/suppliers/quarterly_transactions', 
                { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant',  Accept : "application/json"} })
               .then(handleSuccess, function(error){ 
               ToastService.Show("An unexpected error occured", error); 
               });   

      }

      function GetBranchSummary(){ //using
          return $http.get('http://52.77.228.111/api/v1/suppliers', 
                { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant',  Accept : "application/json"} })
               .then(handleSuccess, function(error){ 
               ToastService.Show("An unexpected error occured", error); 
               });     
      }

      function SearchBranchSummary(startDate, endDate){ //using
         return $http.get('http://52.77.228.111/api/v1/suppliers/?search[startDate]='+startDate+'&search[endDate]='+endDate, 
                { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant',  Accept : "application/json"} })
               .then(handleSuccess, function(error){ 
               ToastService.Show("An unexpected error occured", error); 
               });     
      }


     


      /*
         Public Functions
         ----------------
         Redemption
      */ 


      function GetSupplierRedemption(){ //using
         return $http.get('http://52.77.228.111/api/v1/redemptions', 
             { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant',  Accept : "application/json"} })
            .then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
            });    
      }

      function ShowSupplierRedemption(startDate, endDate){ //using

         return $http.get('http://52.77.228.111/api/v1/redemptions/?search[startDate]='+startDate+'&search[endDate]='+endDate, 
             { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant',  Accept : "application/json"} })
            .then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
            });    
      }

      function GetCustomerRedemption(){ //using
         return $http.get('http://52.77.228.111/api/v1/redemptions/customer_redemptions', 
            { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant',  Accept : "application/json"} })
            .then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
            });    
      }

      function ShowCustomerRedemption(startDate, endDate){ //using
         return $http.get('http://52.77.228.111/api/v1/redemptions/customer_redemptions/?search[startDate]='+startDate+'&search[endDate]='+endDate, 
            { headers: {'X-Access-Token': $rootScope.globals.currentUser.token, 'X-Access-Type': 'Merchant',  Accept : "application/json"} })
            .then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
            });    
      }




      /*
         Public Functions
         ----------------
         JSON
      */

      function GetTableRecords(table){
         var url = DataLink.report_link+"function=gettablerecords&table="+table;
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });         
      }

      function GetRecord(table, dataID){
         var url = DataLink.report_link+"function=getrecord&table="+table+"&brandID="+dataID;
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });         
      }
 

		// private functions
      function handleSuccess(response) {  
         return response.data;
      } 

	}

})();