(function(){
	'use strict';

	angular
		.module('app') 
		.factory('ExportService', ExportService);

	ExportService.$inject = ['$http', '$rootScope', 'DataLink', 'ToastService'];
	function ExportService($http, $rootScope, DataLink, ToastService){
		var service = {};

		// Downloads 

      // Card Registration
      service.ExportCardRegistration_Summary = ExportCardRegistration_Summary; 
      service.ExportActive_CardHistory = ExportActive_CardHistory;
      service.ExportInactive_CardHistory = ExportInactive_CardHistory;
      service.ExportExpired_CardHistory = ExportExpired_CardHistory;

		// Demographics 
		service.ExportDemographics = ExportDemographics; 
      service.ExportCustomerTransactionHistory = ExportCustomerTransactionHistory; 

      // Sales 
      service.ExportSalesSummary = ExportSalesSummary;
      service.ExportSalesPerHour = ExportSalesPerHour;
      service.ExportSalesPerBranch = ExportSalesPerBranch;

      // Spend 
      service.ExportSpendSummary = ExportSpendSummary;

      // Branches
      service.ExportBranchSummary = ExportBranchSummary;
      service.ExportQuarterlySales = ExportQuarterlySales;
      service.ExportQuarterlySalesAll = ExportQuarterlySalesAll;
      service.ExportQuarterlyVisits = ExportQuarterlyVisits;
      service.ExportQuarterlyVisitsAll = ExportQuarterlyVisitsAll; 

      // Customers
      service.ExportCustomerQuarterlySales = ExportCustomerQuarterlySales;
      service.ExportCustomerQuarterlySalesAll = ExportCustomerQuarterlySalesAll;
      service.ExportCustomerQuarterlyVisits = ExportCustomerQuarterlyVisits;
      service.ExportCustomerQuarterlyVisitsAll = ExportCustomerQuarterlyVisitsAll;
      service.ExportCustomerSummary = ExportCustomerSummary; 

      // Vouchers 
      service.ExportBranchRedemptionVouchers = ExportBranchRedemptionVouchers;
      service.ExportCustomerRedemptionVouchers = ExportCustomerRedemptionVouchers;

      // Redemptions 
      service.ExportBranchRedemption = ExportBranchRedemption;
      service.ExportCustomerRedemption = ExportCustomerRedemption;
      service.ExportBranchTransaction = ExportBranchTransaction;

      // Json
      service.GetTableRecords = GetTableRecords;
      service.GetRecord = GetRecord;


		return service; 


      /*
         Public Functions
         ----------------
         Card Registration
      */

      function ExportCardRegistration_Summary(startDate, endDate){
         var url = DataLink.export_link+"function=export_cardregistration_summary&startDate="+startDate+"&endDate="+endDate; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });  
      }
 
      function ExportActive_CardHistory(startDate, endDate){
         var url = DataLink.export_link+"function=export_cardhistory_active&startDate="+startDate+"&endDate="+endDate;   
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });  
      }

      function ExportInactive_CardHistory(startDate, endDate){
         var url = DataLink.export_link+"function=export_cardhistory_inactive&startDate="+startDate+"&endDate="+endDate;  
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });  
      }

      function ExportExpired_CardHistory(startDate, endDate){
         var url = DataLink.export_link+"function=export_cardhistory_expired&startDate="+startDate+"&endDate="+endDate;  
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });  
      }



      /*
			Public Functions
			----------------
			Demographics
      */ 

		function ExportDemographics(startDate, endDate, email, gender, birthday, startAge, endAge){  
         // console.log(startDate + ' ' + endDate + ' ' + email + ' ' + gender + ' ' + birthday + ' ' + startAge + ' ' + endAge)
			var url = DataLink.export_link+"function=export_userInformation&startDate="+startDate+"&endDate="+endDate
				+"&email="+email+"&gender="+gender+"&birthday="+birthday+"&startAge="+startAge+"&endAge="+endAge; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });
		}   

      function ExportCustomerTransactionHistory(memberID){
         var url = DataLink.export_link+"function=export_customerTransactionHistory&memberID="+memberID; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });
      }

      function ExportCustomerDetails(email){
         var url = DataLink.export_link+"function=export_customerProfileDetail&email="+email; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });
      }
 

      /*
         Public Functions
         ----------------
         Loyalty Sales
      */ 

      function ExportSalesSummary(startDate, endDate, email, cardno, fname, lname){
         var url = DataLink.export_link+"function=export_salessummary&startDate="+startDate+"&endDate="+endDate
            +"&email="+email+"&cardno="+cardno+"&fname="+fname+"&lname="+lname; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });         
      }

      function ExportSalesPerHour(startDate, endDate, locID){
         var url = DataLink.export_link+"function=export_salesperhourly&startDate="+startDate+"&endDate="+endDate+"&locID="+locID; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });         
      }

      function ExportSalesPerBranch(startDate, startTime, locID){
         var url = DataLink.export_link+"function=export_salesperbranch&startDate="+startDate+"&startTime="+startTime+"&locID="+locID; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });         
      } 


      /*
         Public Functions
         ----------------
         Spend
      */  
      function ExportSpendSummary(startDate, endDate){
         var url = DataLink.export_link+"function=export_spentsummary&startDate="+startDate+"&endDate="+endDate; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      } 

      function ExportSpendSummary(startDate, endDate){
         var url = DataLink.export_link+"function=export_spentsummary&startDate="+startDate+"&endDate="+endDate; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      }


      /*
         Public Functions
         ----------------
         Branches
      */  
      function ExportBranchSummary(startDate, endDate){
         var url = DataLink.export_link+"function=export_branchsalessummary&startDate="+startDate+"&endDate="+endDate; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      } 

      function ExportQuarterlySales(){
         var url = DataLink.export_link+"function=export_branch_quarterlysales"; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      }

      function ExportQuarterlySalesAll(){
         var url = DataLink.export_link+"function=export_branch_quarterlysales_all"; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      }

      function ExportQuarterlyVisits(){
         var url = DataLink.export_link+"function=export_branch_quarterlyvisits"; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      }

      function ExportQuarterlyVisitsAll(){
         var url = DataLink.export_link+"function=export_branch_quarterlyvisits_all"; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      } 


      /*
         Public Functions
         ----------------
         Customers
      */   
      function ExportCustomerSummary(startDate, endDate){
         var url = DataLink.export_link+"function=export_customer_transactions&startDate="+startDate+"&endDate="+endDate; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      }

      function ExportCustomerQuarterlySales(){
         var url = DataLink.export_link+"function=export_customers_quarterlysales"; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      }

      function ExportCustomerQuarterlySalesAll(){
         var url = DataLink.export_link+"function=export_customers_quarterlysales_all"; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      }

      function ExportCustomerQuarterlyVisits(){
         var url = DataLink.export_link+"function=export_customers_quarterlyvisits"; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      }

      function ExportCustomerQuarterlyVisitsAll(){
         var url = DataLink.export_link+"function=export_customers_quarterlyvisits_all"; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      }


      /*
         Public Functions
         ----------------
         Vouchers
      */    
      function ExportBranchRedemptionVouchers(startDate, endDate){
         var url = DataLink.export_link+"function=export_redemptionVoucher&startDate="+startDate+"&endDate="+endDate; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });             
      }

      function ExportCustomerRedemptionVouchers(startDate, endDate){
         var url = DataLink.export_link+"function=export_customerRedemptionVoucher&startDate="+startDate+"&endDate="+endDate; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      }

   


      /*
         Public Functions
         ----------------
         Redemptions
      */    
      function ExportBranchRedemption(startDate, endDate){
         var url = DataLink.export_link+"function=export_branchRedemption&startDate="+startDate+"&endDate="+endDate; 
         console.log(url);
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      }

      function ExportCustomerRedemption(startDate, endDate){
         var url = DataLink.export_link+"function=export_customerRedemption&startDate="+startDate+"&endDate="+endDate; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      }

      function ExportBranchTransaction(startDate, endDate, locname){
         var url = DataLink.export_link+"function=export_locationTransactions&startDate="+startDate+"&endDate="+endDate+"&locName="+locname; 
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });    
      }


      /*
         Public Functions
         ----------------
         JSON
      */ 
      function GetTableRecords(table){
         var url = DataLink.export_link+"function=gettablerecords&table="+table;
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });         
      }

      function GetRecord(table, dataID){
         var url = DataLink.export_link+"function=getrecord&table="+table+"&brandID="+dataID;
         return $http.get(url).then(handleSuccess, function(error){ 
            ToastService.Show("An unexpected error occured", error); 
         });         
      }
 


		// private functions
      function handleSuccess(response) {  
         return response.data;
      } 

	}

})();